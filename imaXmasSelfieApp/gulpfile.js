/*------------------------------------*\
    IMA template gulpfile
\*------------------------------------*/

var gulp = require('gulp'),
    gulpif = require('gulp-if'),
    argv = require('yargs').argv,
    compass = require('gulp-compass'),
    notify = require('gulp-notify'),
    plumber = require('gulp-plumber'),
    livereload = require('gulp-livereload'),
    jshint = require('gulp-jshint'),
    jsConcat = require('gulp-concat'),
    jsUglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
    map = require('map-stream'),
    imagemin = require('gulp-imagemin'),
    handlebars = require('gulp-compile-handlebars'),
    cordova = require("cordova-lib").cordova,
    run = require('gulp-run-command').default;

/**
 * Flag for build type
 *   $gulp --production
 *   if true:
 *   1. uglify.js will be created
 *   2. stylesheets will be minified
*/
var isDevelopment = argv.development ? true : false;

/**
 * JS files used
 */
var compiledJsFiles = ['www/js/lib/jquery-2.2.1.min.js', 'www/js/lib/jquery.validate.min.js', 'www/js/lib/fastclick.js', 'www/js/lib/handlebars.runtime-v4.0.11.js', 'www/js/lib/pageslider.js', 'www/js/database/appData.js', 'www/js/lib/jquery.shuffleLetters.js', 'www/js/index.js'];

/*------------------------------------*\
    Error handlers
\*------------------------------------*/

/**
 * SASS compilation (compass)
*/
var compassError = function(err) {
    notify.onError({
        title:    "CSS Error",
        subtitle: "Your have a stylesheet error",
        message:  "Error: <%= error.message %>"
    })(err);
    this.emit('end');
};

/**
 * JS compilation
*/
var javaScriptError = function(err) {
    notify.onError({
        title:    'JS Error',
        subtitle: "JavaScript error",
        message:  "Error: <%= error.message %>"
    })(err);
    this.emit('end');
};


/*------------------------------------*\
    Error reporters
\*------------------------------------*/

/**
 * JS
*/
var javaScriptReporter = function(file,cb){
    return map(function(file, cb) {
      if (!file.jshint.success) {
        console.log('JSHINT fail in '+file.path);
        file.jshint.results.forEach(function (err) {
        if (err) {
            gulp.src('/').pipe(notify({
                title: 'JS Error',
                subtitle: 'Javascript error',
                message:  'error on line :' + err.error.line + ':' + err.error.reason
            }));
        }
        });
      }
      cb(null, file);
    });
};


/*------------------------------------*\
    Gulp tasks
\*------------------------------------*/

/**
 * Compass
*/
gulp.task('compass', function() {
    gulp.src('www/css/sass/theme.scss')
    .pipe(plumber({errorHandler: compassError}))
    .pipe(compass({
        css: 'www/css/',
        sass: 'www/css/sass/',
        image: 'www/img/',
        style: isDevelopment ? "expanded" : "compressed"
      }))
    .pipe(gulp.dest('www/css/'))
    .pipe(livereload());
});

/**
 * JS Errors
*/
gulp.task('javaScriptErrors', function(){
    return gulp.src('www/js/*.js')
    .pipe(plumber({errorHandler: javaScriptError}))
    .pipe(jshint())
    .pipe(javaScriptReporter())
    .pipe(livereload())
});

/**
 * JS Manage
*/
gulp.task('JavaScriptManage', function(){
    return gulp.src(compiledJsFiles)
           .pipe(jsConcat('app.js'))
           .pipe(gulpif(!isDevelopment, jsUglify()))
           .pipe(gulp.dest('www/js/dist'));
});

/**
 * Image optimisation
*/
gulp.task('imagemin', function () {
    return gulp.src('www/img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('www/img'))
        .pipe(livereload());
});

/**
 * Handlebars Tasks
*/

gulp.task('htmlBuild', function () {
  return gulp.src('www/src/pages/*.handlebars')
    .pipe(handlebars({}, {
      ignorePartials: true,
      batch: ['www/src/partials']
    }))
    .pipe(rename({
      extname: '.html'
    }))
    .pipe(gulp.dest('www/'));
});

gulp.task('templateManage', run('handlebars  www/src/partials/includes/ -f www/js/dist/templates.js'))

/**
 * Cordova Build Tasks
*/
gulp.task('cordovaDevBuild', ['JavaScriptManage'], run('cordova build'))

gulp.task("cordovaProductionBuild", ['JavaScriptManage'], function (callback) {
    cordova.build({
        "platforms": ['ios'],
        "options": ['--device']
    }, callback)
});

/**
 * Watch files and run gulp tasks
*/
gulp.task('watch', function() {
    gulp.watch('www/css/sass/**/*.*', ['compass', 'cordovaDevBuild']);
    gulp.watch('www/images/*', ['imagemin']);
    gulp.watch('www/src/**/*.handlebars', ['htmlBuild', 'templateManage', 'cordovaDevBuild']);
    gulp.watch('www/js/*.*', ['javaScriptErrors', 'JavaScriptManage', 'cordovaDevBuild']);
    gulp.watch('www/js/lib/*.*', ['javaScriptErrors', 'JavaScriptManage', 'cordovaDevBuild']);

    livereload.listen();
});

gulp.task('default', ['watch']);
