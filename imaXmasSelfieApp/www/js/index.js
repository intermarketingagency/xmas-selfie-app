/**
 * App specific JavaScript
 */
var app = {
    initialize: function() {
        this.bindEvents();
        appData.initAppData(campaignName, campaignUrl);
    },
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
        document.addEventListener("online", this.onOnline, false);
    },
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    onOnline: function() {
        app.receivedOnlineEvent('online');
    },
    receivedEvent: function(id) {
        appData.initDataBase();
        app.initAppCache();
        app.initClickEvents();
        app.initDependentPlugins();
        app.initLoadApp();

        // hide the ios status bar
        StatusBar.hide();
    },
    receivedOnlineEvent: function(id) {
        appData.initDataBase();
        appData.initIsOnline();
    },
    /**
    * Initialises the apps cached elements
    */
    initAppCache: function(){
        appCache = {
            $window: $(window),
            $body: $('body'),
            slider: new PanelSlider($('body')),
            homeTpl: Handlebars.templates.home,
            cameraTpl: Handlebars.templates.camera,
            initiateTpl: Handlebars.templates.initiating,
            verifyingTpl: Handlebars.templates.verifying,
            identifiedTpl: Handlebars.templates.identified,
            termsTpl: Handlebars.templates.terms,
            termslistTpl: Handlebars.templates.termslist,
            accessdeniedTpl: Handlebars.templates.accessdenied,
            accessgrantedTpl: Handlebars.templates.accessgranted,
            cameraOptions: {
                x: 0,
                y: 0,
                width: window.screen.width,
                height: window.screen.height,
                camera: CameraPreview.CAMERA_DIRECTION.FRONT,
                toBack: true,
                tapPhoto: true,
                tapFocus: false,
                previewDrag: false
            }
        };
    },
    /**
    * Initialises jQuery plugins
    */
    initDependentPlugins: function(){
        // eliminates 300ms on touch ui
        document.addEventListener('DOMContentLoaded', function() {
            FastClick.attach(document.body);
        }, false);

        // jQuery validate method for emails
        $.validator.methods.email = function( value, element ) {
            return this.optional( element ) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test( value );
        };
    },
    /**
    * Initialises Click Events
    */
    initClickEvents: function() {
        //when page navigation is clicked
        appCache.$window.on('hashchange', app.pageSlider);
    },
    /**
    * Initialises page slider func
    */
    pageSlider: function(){
        var page,
            hash = window.location.hash;

        if (hash == "#camera") {
            appCache.slider.slidePanel(appCache.cameraTpl());
            app.initCamera();
        } else if (hash == "#initiate") {
            appCache.slider.slidePanel(appCache.initiateTpl());
            app.initScanner();
        } else if (hash == "#verifying") {
            appCache.slider.slidePanel(appCache.verifyingTpl());
            app.initVerifying();
        } else if (hash == "#identified") {
            appCache.slider.slidePanel(appCache.identifiedTpl());
            app.initIdentified();
        } else if (hash == "#terms") {
            appCache.slider.slidePanel(appCache.termsTpl());
            app.initTerms();
        } else if (hash == "#termslist") {
            appCache.slider.slidePanel(appCache.termslistTpl());
            app.initTermsList();
        }  else if (hash == "#accessdenied") {
            appCache.slider.slidePanel(appCache.accessdeniedTpl());
            app.initAccessFlash();
        } else if (hash == "#accessgranted") {
            appCache.slider.slidePanel(appCache.accessgrantedTpl());
            app.initAccessFlash();
        } else if (hash == "#restart") {
            appCache.slider.slidePanel(appCache.homeTpl());
            app.initRestart();
        } else {
            $('.container__title').removeClass('hide');
            app.initLoadApp();
        }
    },
    /**
    * Initialises the connect page
    */
    initLoadApp: function() {

        // load home page
        appCache.slider.slidePanel(appCache.homeTpl());
        CameraPreview.startCamera(appCache.cameraOptions);

        app.titleShuffle();

        setTimeout(function(){
            $('body .button').addClass('is-active');
        }, 1000);

        setTimeout(function(){
            $('body .button').addClass('has-loaded');
        }, 2000);
    },
    /**
    * Initialises restart
    */
    initRestart: function() {

        app.titleShuffle();

        setTimeout(function(){
            $('body .button').addClass('is-active');
        }, 1000);

        setTimeout(function(){
            $('body .button').addClass('has-loaded');
        }, 2000);
    },
    /**
    * Initialises the camera ppage
    */
    initCamera: function() {
        $('.background').addClass('hide');
        setTimeout(function(){
            CameraPreview.takePicture(function(base64PictureData){
                console.log('picture taken');
            });
        }, 2000);
        setTimeout(function(){
            window.location.hash = 'verifying';
        }, 3000);
    },
    /**
    * Initialises the scanner page
    */
    initScanner: function() {
        $('body .js-title-initiating').shuffleLetters({
            "text": "_INITIATING_FACIAL_SCANNER_"
        });
        $('.facial-scanner').addClass('is-active');
        setTimeout(function(){
            window.location.hash = 'camera';
        }, 3000);
    },
    /**
    * Initialises the verifying page
    */
    initVerifying: function() {
        $('body .js-title-verifying').shuffleLetters({
            "text": "_VERIFYING_IDENTITY_"
        });
        $('.background').removeClass('hide');
        setTimeout(function(){
            $('body .box').addClass('is-active');
        }, 1000);
        setTimeout(function(){
            window.location.hash = 'identified';
        }, 4000);
    },
    /**
    * Initialises the identified page
    */
    initIdentified: function() {
        $('body .js-title-identified').shuffleLetters({
            "text": "SUBJECT_****7347859****"
        });
        setTimeout(function(){
            window.location.hash = 'terms';
        }, 3000);
    },
    /**
    * Initialises the terms page
    */
    initTerms: function() {
        $('.container__title').addClass('hide');
        $('body .js-terms-title').shuffleLetters({
            "text": "!_IMPORTANT_INFORMATION_!"
        });

        setTimeout(function(){
            window.location.hash = 'termslist';
        }, 4000);
    },
    /**
    * Initialises the termslist page
    */
    initTermsList: function() {
        setTimeout(function(){
            $('body .termslist-title').addClass('is-active');
        }, 3000);

        setTimeout(function(){
            if(app.getAccess()) {
                window.location.hash = 'accessgranted';
            } else {
                window.location.hash = 'accessdenied';
            }
        }, 4000);
    },
    /**
    * Initialises the denied page
    */
    initAccessFlash: function() {
        $('.container').addClass('denied');

        setTimeout(function(){
            $('.container').removeClass('denied');
            window.location.hash = 'home';
        }, 3000);
    },
    /**
    * Initialises the granted page
    */
    initAccessGranted: function() {
        setTimeout(function(){
            window.location.hash = 'home';
        }, 3000);
    },
    /**
    * Animate Title
    */
    titleShuffle: function() {
        $('.js-title-global').shuffleLetters({
            "text": "BIO_RCOGNITION SYSTM"
        });
    },

    getAccess: function () {
        return Math.random() >= 0.1;
    },
};
app.initialize();
