// MUST DECLARE DB OUTSIDE OF SITECACHE
var $db = null;

var appData = {
    initAppData: function(campaignName, campaignUrl) {
        this.campaignName = campaignName;
        this.campaignUrl = campaignUrl;
    },
    initDataBase: function() {

        if(!$db) {

            $db = window.sqlitePlugin.openDatabase({name: this.campaignName + '.db', location: 1}, function(db) {
              db.transaction(function(tx) {
                    tx.executeSql('CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY, content TEXT)');
                    tx.executeSql('SELECT COUNT(*) AS totalUsers FROM users', [], function(tx, res) {
                        dbInitialRows = res.rows.item(0).totalUsers;
                        $('#post-counter').html(dbInitialRows);
                    });
              }, function(err) {

              });
            });
        }
    },
    storeDataLocally: function(campaignData) {

        $db.transaction(function(tx) {

            tx.executeSql('INSERT INTO users (content) VALUES (?)', [JSON.stringify(campaignData)], function(tx, res) {

                if (appData.isOnline()){
                    appData.initIsOnline(1);
                }
            });

        }, function(error) {
            console.log('Transaction ERROR: ' + error.message);
          }, function() {
            console.log('Populated database OK');
          });

        $db.transaction(function(tx) {
            tx.executeSql('SELECT COUNT(*) AS totalUsers FROM users', [], function(tx, res) {
                dbRows = res.rows.item(0).totalUsers;
                $('#post-counter').html(dbRows);
            });
        });
    },
    isOnline: function() {

        var networkState     = navigator.connection.type,
            connectionOnline = true;

        var states = {};
        states[Connection.UNKNOWN]  = 'offline';
        states[Connection.NONE]     = 'offline';

        connectionOnline = (states[networkState] !== 'offline');

        return connectionOnline;
    },
    initIsOnline: function(limit) {
        var sendingEmail = false;
        var offset = 0;

        if (sendingEmail) {
            return;
        }

        if (!limit) {
            limit = 100;
            //Check how many total rows we have on the DB
            $db.transaction(function(tx) {
                tx.executeSql('SELECT COUNT(*) AS totalUsers FROM users', [], function(tx, res) {
                    numberOfRows = res.rows.item(0).totalUsers;
                });
            });
        } else {
            //If limit is set only output that one so it doesn't send everything
            numberOfRows = limit;
        }

        $db.transaction(function(tx) {

            //Send in batches
            while (offset < numberOfRows) {
                tx.executeSql('SELECT * FROM users LIMIT ? OFFSET ?', [limit, offset], function(tx, res) {
                    for (var i = 0; i < res.rows.length; i++) {
                        var $id       = res.rows.item(i).id,
                            $content  = res.rows.item(i).content;

                        (function($id) {

                            $.ajax({
                                type: 'POST',
                                url: appData.campaignUrl,
                                data: $content,
                                processData: false,
                                beforeSend: function(xhr) {
                                    xhr.setRequestHeader("Content-Type", "application/json" );
                                    xhr.setRequestHeader ("Authorization", "Basic " + btoa(campaignAPIKey + ':x'));
                                    $sendingEmail = true;
                                },
                                success: function(result) {
                                    $db.transaction(function(tx) {
                                        tx.executeSql('DELETE FROM users WHERE id = ' + $id);
                                        tx.executeSql('SELECT COUNT(*) AS totalUsers FROM users', [], function(tx, res) {
                                            dbUpdatedRows = res.rows.item(0).totalUsers;
                                            $('#post-counter').html(dbUpdatedRows);
                                        });
                                    });
                                },
                                error: function(result) {
                                    console.log(result);
                                    if(result.status === 400) {
                                        $db.transaction(function(tx) {
                                            tx.executeSql('DELETE FROM users WHERE id = ' + $id);
                                            tx.executeSql('SELECT COUNT(*) AS totalUsers FROM users', [], function(tx, res) {
                                                dbUpdatedRows = res.rows.item(0).totalUsers;
                                                $('#post-counter').html(dbUpdatedRows);
                                            });
                                        });
                                    }
                                },
                                complete: function() {
                                    $sendingEmail = false;
                                }
                            });

                        })($id);
                    }
                });

                offset += limit;
            }
        });
    }
};
