function PanelSlider(container) {

    var container = container,
        $currentPanel,
        stateHistory = [];

    // Use this function if you want PanelSlider to automatically determine the sliding direction based on the state history
    this.slidePanel = function(panel) {

        var l = stateHistory.length,
            state = window.location.hash;

        if (l === 0) {
            stateHistory.push(state);
            this.slidePanelFrom($(panel));
            return;
        }
        if (state === stateHistory[l-2]) {
            stateHistory.pop();
            this.slidePanelFrom($(panel), 'left');
        } else {
            stateHistory.push(state);
            this.slidePanelFrom($(panel), 'right');
        }

    };

    // Use this function directly if you want to control the sliding direction outside PanelSlider
    this.slidePanelFrom = function(panel, from) {

        container.append($(panel));

        if (!$currentPanel || !from) {
            $(panel).attr("class", "panel center");
            $currentPanel = $(panel);
            return;
        }

        // Position the panel at the starting position of the animation
        $(panel).attr("class", "panel " + from);

        $currentPanel.one('webkitTransitionEnd', function(e) {
            $(e.target).remove();
        });

        // Force reflow.
        container[0].offsetWidth;

        // Position the new panel and the current panel at the ending position of their animation with a transition class indicating the duration of the animation
        $(panel).attr("class", "panel transition center");
        $currentPanel.attr("class", "panel transition " + (from === "left" ? "right" : "left"));
        $currentPanel = $(panel);
    };

}